npm create astro@latest

# Preact

    npx astro add preact

# Add MDX
https://docs.astro.build/en/guides/integrations-guide/mdx/
    
    npx astro add mdx

npm run start


# Add sharing state (WebApp)
https://docs.astro.build/en/core-concepts/sharing-state/
https://github.com/nanostores/nanostores#smart-stores
https://github.com/nanostores/persistent#persistent-engines

    npm install nanostores @nanostores/preact
    npm install nanostores @nanostores/persistent

# Add node SSR

    npx astro add node

# Add CSS

    npx astro add tailwind

# BINARY Bundle

    npm install --save-dev caxa
    npx caxa  --input "."   --output "caxa.exe"  -- "{{caxa}}/node_modules/.bin/node"  "{{caxa}}/dist/server/entry.mjs"


# Missed templates

- md(x) child layout
- Persisted store
- SSR 
- https://docs.astro.build/en/guides/markdown-content/#importing-markdown
- Api
  - from astro
  - from MD
  - from MD