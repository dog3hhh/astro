
import { useState } from 'preact/hooks'

// export default function Counter() {
//   const [count, setCount] = useState(0)

//   return (
//     <div className='counter'>
//       <button onClick={() => setCount(count - 1)} disabled={count === 0}>
//         -
//       </button>
//       <h2>{count}</h2>
//       <button onClick={() => setCount(count + 1)}>+</button>
//     </div>
//   )
// }

import { useStore } from '@nanostores/preact';
import { countState, getCartItem , addCartItem } from './State';
// import { useState } from 'preact/hooks'


export default function Counter() {

  // const [count, setCount] = useState(0)
  const $countState: number = useStore(countState);
  // const $getCartItem = useStore(getCartItem);
  // const $addCartItem = useStore(addCartItem);

  return (
    <div className='counter'>
      <button onClick={() => { 
        countState.set(parseInt($countState) - 1)
        }
        } disabled={$countState === 0}>
        -
      </button>
      <h2>{getCartItem()}</h2>
      <button onClick={() => { 
            countState.set(1 + parseInt($countState))
        }
       }>+</button>
    </div>
  )
}
