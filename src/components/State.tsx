// import { atom, map } from 'nanostores';
import { persistentAtom as atom } from '@nanostores/persistent';

export const countState:number = atom< number >(0);

export function addCartItem({ newCount  }) {
    countState.set(newCount)
}

export function getCartItem() {
    return countState.get()
    // return Math.random();
}
