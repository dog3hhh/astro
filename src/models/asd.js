var page = 1;
var condition = null;
var totalPages = 1;
async function next(renderPage) {
  do {
    const url =
      "http://127.0.0.1/wp-json/wp/v2/posts?per_page=2&_fields=author,id,excerpt,title,link" +
      "&page=" +
      page;
    const res = await fetch(url);
    totalPages = res.headers.get("x-wp-totalpages");
    const posts = await res.json();
    await renderPage(posts);
    page++;
  } while (page < totalPages);
}

next(async function (posts) {
  console.log(posts);
});
