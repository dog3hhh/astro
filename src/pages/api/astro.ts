import { APIContext } from 'astro';

export function get({ cookies }: APIContext) {
	let userId = cookies.get('user-id').value;
	return {
		body: JSON.stringify({ cookies: cookies }),
	};
}