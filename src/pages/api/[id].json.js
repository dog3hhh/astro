export const prerender = false;


export async function get({ params }) {
  return new Response(JSON.stringify("dynamic"), {
    status: 200,
    headers: {
      "Content-Type": "application/json"
    }
  });
}